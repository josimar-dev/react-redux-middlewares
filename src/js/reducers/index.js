import { ADD_ARTICLE, DATA_LOADED } from '../constants/action-types';

const initialState = {
  articles: [],
  remoteArticles: [],
};

function rootReducer(state = initialState, action) {
  const { type, payload } = action;
  const { articles, remoteArticles } = state;
  if (type === ADD_ARTICLE) {
    return {
      ...state,
      articles: [...articles, payload],
    };
  }
  if (type === DATA_LOADED) {
    return {
      ...state,
      remoteArticles: [...remoteArticles, ...payload],
    }
  }
  return state;
}

export default rootReducer;