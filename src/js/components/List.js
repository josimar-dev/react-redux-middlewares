import React from 'react';
import { connect } from 'react-redux';

const ConnectedList = ({ articles }) => (
  <ul>
    {articles.map(el => (
      <li key={el.id}>{el.title}</li>
    ))}
  </ul>
);

const mapStateToProps = state => {
  const { articles } = state;
  return { articles };
}

const List = connect(mapStateToProps)(ConnectedList);
export default List;