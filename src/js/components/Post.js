import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { getData } from '../actions';

function mapStateToProps(state) {
  const { remoteArticles } = state;
  return { remoteArticles };
}

function mapDispatchToProps(dispatch) {
  return { getData: () => dispatch(getData()) };
}

const ConnectedPost = props => {
  const { getData, remoteArticles } = props;
  useEffect(() => {
    getData();
  },[]);
  
  return(
    <ul className="list-group list-group-flush">
      {remoteArticles.map(el => (
        <li className="list-group-item" key={el.id}>{el.title}</li>
      ))}
    </ul>
  );
};

const Post = connect(mapStateToProps, mapDispatchToProps)(ConnectedPost);

export default Post;