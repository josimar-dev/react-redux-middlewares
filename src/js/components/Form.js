import React, { useState } from 'react';
import { connect } from 'react-redux';

import { addArticle } from '../actions/index';

function mapDispatchToProps(dispatch) {
  return { addArticle: article => dispatch(addArticle(article)) };
}


const ConnectedForm = props => {
  const [ formData, setFormData ] = useState({ id: '', title: '' });

  const handleChange = event => {
    const { target } = event;
    const { id, value } = target;
    setFormData({ id, title: value });
  }

  const handleSubmit = event => {
    event.preventDefault();
    const { addArticle } = props;
    addArticle(formData);
    setFormData({ id: '', title: '' });
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="title">Title</label>
        <input
          type="text"
          id="title"
          value={formData.title}
          onChange={handleChange}
        />
      </div>
      <button type="submit">SAVE</button>
    </form>
  );
};

const Form = connect(null, mapDispatchToProps)(ConnectedForm);

export default Form;

